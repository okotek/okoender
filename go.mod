module gitlab.com/okotek/okoender

go 1.13

replace gitlab.com/okotek/okonet => ../okonet

require (
	gitlab.com/okotek/okoframe v0.0.0-20220105061727-f995ae7b7d70
	gitlab.com/okotek/okonet v0.0.0-00010101000000-000000000000
	gocv.io/x/gocv v0.29.0
)
