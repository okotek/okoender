package main

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/okotek/okoframe"
	"gitlab.com/okotek/okonet"
	"gocv.io/x/gocv"
)

/*
	End-phase pipeline stage for testing okokit. Ender simply dumps the recieved data.
*/

func main() {
	///mt.Println("I'm alive!!!

	inchan := make(chan okoframe.Frame)

	go dumpFrames(0, inchan)
	okonet.ReceiverHandler(inchan, ":8082")

	//go dumpFrames(0, inchan)

	for {
		time.Sleep(10000 * time.Hour)
	}

}

func _dumpFrames(mode int, input chan okoframe.Frame) {
	for {
		tmp := <-input

		fmt.Println("tmpLn:", len(tmp.ImDat))
	}
}

func dumpFrames(mode int, input chan okoframe.Frame) {

	//primeFrame := <-input

	//fmt.Println("\n\n\nGot first\n\n\n", len(primeFrame.ImDat)-len(primeFrame.ImDat))
	//os.Exit(0)
	for frm := range input {
		//fmt.Printf("\n\n In loop in ender. \n\nMode :", mode)
		fmt.Println("Motion Percent:", frm.MotionPercentChange)

		go func() {
			fmt.Println(frm.Channels, "chan is my favorite imageboard.")
			tmp := okoframe.CreateMatFromFrameSimp(frm)

			strTm := strconv.Itoa(int(time.Now().UnixNano()))
			fmt.Println("============================MPC:\n", frm.MotionPercentChange)
			fmt.Println("Ready to write")
			gocv.IMWrite("output"+strTm+"_"+frm.ImgName+".jpg", tmp)
			return
		}()
	}
}
